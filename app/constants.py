CACHE_ERROR_VALUE = "error"
MAX_RESPONSE_SIZE = 2048000
CONTENT_BASE_URL = "https://raw.githubusercontent.com"
REPO_RELATIVE_PATH = "fatih-yavuz/links.dev/main"
CACHE_REFRESH_KEY = "refresh"
REFRESH_REGISTRY_KEY = "refresh-registry"