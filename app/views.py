from typing import Union

import yaml
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django_ratelimit.decorators import ratelimit
from django_ratelimit.exceptions import Ratelimited

from .constants import CACHE_ERROR_VALUE, CONTENT_BASE_URL, REPO_RELATIVE_PATH, CACHE_REFRESH_KEY, REFRESH_REGISTRY_KEY
from .github import GithubContentService
from .utils import decide_content_type
from .view_model_factories import PageViewModelFactory

github_content_service = GithubContentService()


def get_github_username_of(username: str, refresh=False) -> Union[str, bool]:
    try:
        url = f"{CONTENT_BASE_URL}/{REPO_RELATIVE_PATH}/registry.yaml"
        response = github_content_service.get(url, refresh)
        response = response.lower()

        data = yaml.safe_load(response)

        if "users" in data and username in data["users"]:
            return data["users"][username]["github_username"]
        else:
            return False
    except Exception as e:
        print(e)
        return False


@ratelimit(key='ip', rate='1000/m')
def index(request, username):
    refresh = request.GET.get(CACHE_REFRESH_KEY) or False
    refresh_registry = request.GET.get(REFRESH_REGISTRY_KEY) or False
    try:
        username = username.lower()
        github_username = get_github_username_of(username, refresh_registry)
        page_content = github_content_service.get_page_content(github_username, refresh)
        if page_content == CACHE_ERROR_VALUE:
            path = f"{CONTENT_BASE_URL}/{REPO_RELATIVE_PATH}/error-pages/404.html"
            response = github_content_service.get(path, refresh)
            return HttpResponse(response, content_type='text/html', status=404)
        page_view_model = PageViewModelFactory.from_json_text(page_content)
        css = github_content_service.get_css(github_username, refresh)
        if css != CACHE_ERROR_VALUE:
            page_view_model.css = css

        page_view_model.is_verified = True
        page_view_model.username = username
    except Exception as e:
        print(e)
        raise Exception("There is something wrong with the registry")

    return render(request, f'index.html', {
        "data": page_view_model,
    })


@ratelimit(key='ip', rate='1000/m')
def home(request):
    refresh = request.GET.get(CACHE_REFRESH_KEY) or False

    try:

        path = f"{CONTENT_BASE_URL}/{REPO_RELATIVE_PATH}/landing/index.html"
        response = github_content_service.get(path, refresh)
        return HttpResponse(response, content_type='text/html')
    except Exception as e:
        print(e)
        raise Exception("There is something wrong with the registry")


@ratelimit(key='ip', rate='1000/m')
def cdn(request):
    try:
        path = request.GET.get('path')
        if not path:
            return HttpResponse('Path parameter is missing', status=400)

        if 'fatih-yavuz/links.dev/' not in path:
            return HttpResponse('Forbidden', status=403)

        refresh = request.GET.get(CACHE_REFRESH_KEY) or False

        url = f'{CONTENT_BASE_URL}/{path}'
        response = github_content_service.get(url, refresh)

        if response != CACHE_ERROR_VALUE:
            content_type = decide_content_type(path)

            return HttpResponse(response, content_type=content_type)

        else:
            return HttpResponse('Error: Could not retrieve file content', status=500)
    except Exception as e:
        print(e)
        return HttpResponse('Error: Could not retrieve file content', status=500)


def handler403(request, exception=None):
    if isinstance(exception, Ratelimited):
        return HttpResponse('Sorry you are blocked', status=429)
    return HttpResponseForbidden('Forbidden')
