import mock
import zlib

from app.constants import CACHE_ERROR_VALUE
from app.github import GithubContentService
from app.utils import decide_content_type
from unittest import TestCase


class DecideContentTypeTestCase(TestCase):
    def test_decide_content_type(self):
        self.assertEqual(decide_content_type('file.css'), 'text/css')
        self.assertEqual(decide_content_type('file.js'), 'application/javascript')
        self.assertEqual(decide_content_type('file.html'), 'text/html')
        self.assertEqual(decide_content_type('file.xhtml'), 'application/xhtml+xml')
        self.assertEqual(decide_content_type('file.jpg'), 'image/jpeg')
        self.assertEqual(decide_content_type('file.png'), 'image/png')
        self.assertEqual(decide_content_type('file.gif'), 'image/gif')
        self.assertEqual(decide_content_type('file.ico'), 'image/x-icon')
        self.assertEqual(decide_content_type('file.tiff'), 'image/tiff')
        self.assertEqual(decide_content_type('file.svg'), 'image/svg+xml')
        self.assertEqual(decide_content_type('file.pdf'), 'application/pdf')
        self.assertEqual(decide_content_type('file.json'), 'application/json')
        self.assertEqual(decide_content_type('file.ld+json'), 'application/ld+json')
        self.assertEqual(decide_content_type('file.xml'), 'application/xml')
        self.assertEqual(decide_content_type('file.zip'), 'application/zip')
        self.assertEqual(decide_content_type('file.form'), 'application/x-www-form-urlencoded')
        self.assertEqual(decide_content_type('file.csv'), 'text/csv')

        self.assertEqual(decide_content_type('file.unknown'), 'text/plain')


class TestGithubContentService(TestCase):

    def setUp(self):
        # Create a mock cache object
        self.mock_cache = mock.MagicMock()

        # Create a GithubContentService object with the mock cache
        self.service = GithubContentService(cache=self.mock_cache)

    def test_get_cache_hit(self):
        # Test the cache hit scenario
        self.mock_cache.get.return_value = zlib.compress("cache data".encode())
        result = self.service.get("http://example.com")
        self.assertEqual(result, "cache data")

    def test_get_cache_miss(self):
        # Test the cache miss scenario
        self.mock_cache.get.return_value = None
        with mock.patch("requests.get") as mock_get:
            mock_response = mock.MagicMock()
            mock_response.ok = True
            mock_response.text = "data from url"
            mock_get.return_value = mock_response
            result = self.service.get("http://example.com")
            self.assertEqual(result, "data from url")
            self.mock_cache.set.assert_called_once_with(self.service.get_cache_key("http://example.com"),
                                                        self.service.compress("data from url"), None)

    def test_get_cache_miss_error(self):
        # # Test the invalid response scenario
        self.mock_cache.get.return_value = None
        with mock.patch("requests.get") as mock_get:
            mock_response = mock.MagicMock()
            mock_response.ok = False
            mock_response.text = "invalid data"
            mock_get.return_value = mock_response
            result = self.service.get("http://example.com")
            self.assertEqual(result, CACHE_ERROR_VALUE)
            self.mock_cache.set.assert_called_once_with(self.service.get_cache_key("http://example.com"),
                                                        self.service.compress(CACHE_ERROR_VALUE), None)
