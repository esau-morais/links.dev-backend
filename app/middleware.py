header_keys = [
    'REMOTE_ADDR',
    'HTTP_X_FORWARDED_FOR',
    'X_FORWARDED_FOR',
    'HTTP_CLIENT_IP',
    'HTTP_X_REAL_IP',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'HTTP_VIA',
]


def reverse_proxy(get_response):
    def process_request(request):
        for header_key in header_keys:
            if header_key in request.META:
                request.META['REMOTE_ADDR'] = request.META[header_key]
                break

        return get_response(request)

    return process_request
