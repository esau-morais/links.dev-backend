from typing import List


class LinkViewModel:
    id: str
    text: str
    url: str
    icon: str
    color: str


class PageViewModel:
    links: List[LinkViewModel]
    image_url: str
    username: str
    name: str
    is_verified: bool
    theme: str
