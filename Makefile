#!make
include ./.env
export $(shell sed 's/=.*//' ./.env)


local-backend-destroy: login
	docker rmi -f ${BACKEND_IMAGE_NAME}:${BACKEND_IMAGE_TAG} || true

local-backend-build: login local-backend-destroy
	docker build --no-cache -t ${BACKEND_IMAGE_NAME}:${BACKEND_IMAGE_TAG} .

local-backend-push: login
	docker push ${BACKEND_IMAGE_NAME}:${BACKEND_IMAGE_TAG}

local-destroy: local-backend-destroy

local-build: login local-backend-build

local-push: login local-backend-push


login:
	docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASS}

pip-remove-all:
	pip freeze | xargs pip uninstall -y

docker-compose-up: local-destroy
	docker image rm -f links.dev-backend && docker-compose rm -fsv && docker-compose up --force-recreate

prod-docker-compose-create-file:
	envsubst < docker-compose.prod.template.yml > docker-compose.prod.yml

prod-docker-compose-push-file:
	scp -i ${SSH_KEY} docker-compose.prod.yml ${EC2_USER}@${EC2_IP}:/home/${EC2_USER}/docker-compose.yml

prod-prepare-deployment: prod-docker-compose-create-file prod-docker-compose-push-file


make-migrations:
	python manage.py makemigrations

migrate: make-migrations
	python manage.py migrate

create-superuser:
	python manage.py createsuperuser

runserver:
	python manage.py runserver

reset-migrations:
	find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
	find . -path "*/migrations/*.pyc"  -delete
	rm -rf app/migrations
	rm db.sqlite3

test:
	python manage.py test --pattern="*tests.py"