FROM python:3.9

ARG srcDir=.
WORKDIR /app
COPY $srcDir/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY $srcDir .

EXPOSE 8000

CMD ["gunicorn", "app.wsgi", "-b", "0.0.0.0:8000"]