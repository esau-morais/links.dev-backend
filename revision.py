import re
import subprocess
from datetime import datetime

now = datetime.now()

dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
path = "pages/templates/pages/fatih.html"

target = r'<div class="revision">Last deployed at: .*</div>'

with open(path, 'r+') as f:
    content = f.read()
    content = re.sub(target, f'<div class="revision">Last deployed at: {dt_string}</div>', content)
    f.seek(0)
    f.write(content)




